package ru.t1.annenkovsv.tm.util;

public interface MemoryCalculationUtil {

    static long getAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    static String getFreeMemory() {
        return FormatUtil.format(Runtime.getRuntime().freeMemory());
    }

    static String getMaxMemory() {
        return FormatUtil.format(Runtime.getRuntime().maxMemory());
    }

    static String getTotalMemory() {
        return FormatUtil.format(Runtime.getRuntime().totalMemory());
    }

    static String getUsedMemory() {
        return FormatUtil.format(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
    }

}
